module myreg #(parameter n = 1) (output reg  [n - 1:0] w, input  [n - 1:0] parin, input clk, rst, ld);
  always@(posedge clk) begin
    if(rst == 1)
      w = 0;
    else if(ld == 1)
      w = parin;
  end
endmodule