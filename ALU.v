module ALU(output zero ,output reg signed [31:0] c, input [2:0] operation , input signed [31:0] a, b);
	
	parameter [2:0] ADD = 0, SUB = 1, AND = 2, OR = 3, XOR = 4, SLT = 5;
	
	always @( operation ,  b , a) begin
		case (operation)
			AND :  c = a & b;
			OR :   c = a | b; 
			ADD :  c = a + b;
			SUB :  c = a - b;
			SLT :  c = ($signed(a) < $signed(b))? 1 : 0 ;
			XOR : c = a ^ b;
		endcase
	end
	assign zero = (c) ? 0 : 1;
	
endmodule

