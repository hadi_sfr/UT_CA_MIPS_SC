`include "memory.v"
`include "instruction_mem.v"
`include "mips.v"
module microcontroller(input clk, rst);
	wire [31:0] dd, instruction, dmwd, ALU_out;
	wire [11:0] pc_out;
	wire data_mem_write_en;
	datamemory data_mem(dd, ALU_out, dmwd, data_mem_write_en, clk);
	instruction_mem inst_mem(instruction, pc_out, clk);
	mips processor(pc_out, dmwd, ALU_out, data_mem_write_en, rst, clk, instruction, dd);
endmodule