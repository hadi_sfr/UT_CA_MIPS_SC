`include "myreg.v"
`include "registerfile.v"
`include "ALU.v"
`include "division.v"
`include "multiply.v"
module datapath(
	output zero,
	output [5:0] opcode,
	output [31:0] ALU_out, dmwd,
	output [11:0] pc_out,
	input clk, rst, jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c,
	input [2:0] rfw_d_c, ALU_c,
	input [31:0] inst, dd
	);
	wire [31:0] rd1, rd2, rfw_d, hi_out, lo_out, hi_in, lo_in, npc_e, ALU_in, imm;
	wire pc_en, reset;
	wire [11:0] pc_in, npc;
	wire [31:0] jpc, jval;
	wire [31:0] hi_in_mult, hi_in_div, lo_in_mult, lo_in_div;
	assign dmwd = rd1;
	assign pc_in = pc_in_c ? jpc[11:0] : npc;
	assign npc = pc_out + 12'd4;
	assign npc_e = {20'b0, npc};
	assign jpc = $signed(npc_e) + $signed({jval[29:0], 2'b0});
	assign jval = jval_c ? rd2 : imm;
	// assign reset = 1'b0; 
	assign pc_en = 1'b1;
	assign opcode = inst[31:26];
	wire [4:0] rfw_a;
	assign rfw_a = rfw_a_c ? inst[15:11] : inst[20:16];
	myreg #(32) hi(hi_out, hi_in, clk, rst, hilo_en);
	myreg #(32) lo(lo_out, lo_in, clk, rst, hilo_en);
	myreg #(12) pc(pc_out, pc_in, clk, rst, pc_en);
	assign rfw_d = (rfw_d_c == 3'd0) ? dd : (rfw_d_c == 3'd1) ? ALU_out : (rfw_d_c == 3'd2) ? npc_e : (rfw_d_c == 3'd3) ? lo_out : (rfw_d_c == 3'd4) ? hi_out : (rfw_d_c == 3'd5) ? {inst[15:0], 16'b0} : 32'bz;
	registerfile regfile(rd1, rd2, inst[25:21], inst[20:16], rfw_a, rfw_d, clk, rf_write_en);
	assign imm = {{16{inst[15]}}, inst[15:0]};
	assign ALU_in = ALU_in_c ? imm : rd2;
	ALU myALU(zero, ALU_out, ALU_c, rd1, ALU_in);
	// assign dwd = dwd_c ? {inst[15:0], 16'b0} : rd2;
	// datamemory data_mem(dd, ALU_out, rd2, data_mem_write_en, clk);
	// instruction_mem inst_mem(inst, pc_out, clk);
	multiply mult({hi_in_mult, lo_in_mult}, rd1, rd2);
	division div(lo_in_div, hi_in_div, rd1, rd2);
	assign hi_in = hilo_c ? hi_in_div : hi_in_mult;
	assign lo_in = hilo_c ? lo_in_div : lo_in_mult;
endmodule