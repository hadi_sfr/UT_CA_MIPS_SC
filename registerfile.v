module registerfile(output [31:0] read_data1, read_data2, input [4:0] read_addr1, read_addr2, write_addr, input  [31:0] write_data, input clk, write_en);
	reg [31:0] data [0:31];
	initial data[0] <= 0;
	always@(posedge clk)begin
		// read_data1 <= data[read_addr1];
		// read_data2 <= data[read_addr2];
		if(write_en && write_addr != 0)
			data[write_addr] <= write_data;
	end
	assign read_data1 = data[read_addr1];
	assign read_data2 = data[read_addr2];
	// assign data[write_addr] = (write_en && write_addr != 0) ? write_data : data[write_addr];
endmodule