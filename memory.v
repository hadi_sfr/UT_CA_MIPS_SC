module datamemory(output [31:0] read_data, input  [31:0] address, input  [31:0] write_data, input write_en, clk);
	reg [7:0] data [0:5000];
	always@(posedge clk)begin
		if(|address[31:12] == 1)
			;// read_data <= 32'bz;
		else begin
			if(write_en)
				{data[address], data[address + 1], data[address + 2], data[address + 3]} = write_data;
			// read_data <= {data[address], data[address + 1], data[address + 2], data[address + 3]};
		end
	end
	assign read_data = (|address[31:12] == 1) ? 32'bz : {data[address], data[address + 1], data[address + 2], data[address + 3]};
endmodule