module division(output signed [31:0] quotient, remainder, input signed [31:0] a, b);
	assign quotient = $signed(a) / $signed(b);
	assign remainder = $signed(a) % $signed(b);
endmodule