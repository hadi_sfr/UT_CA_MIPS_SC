module multiply(output signed [63:0] out, input signed [31:0] a, b);
	assign out = $signed(a) * $signed(b);
endmodule