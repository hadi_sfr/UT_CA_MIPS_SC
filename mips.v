`include "controller.v"
`include "datapath.v"
module mips(
	output [11:0] pc_out,
	output [31:0] dmwd, ALU_out,
	output data_mem_write_en,
	input rst, clk,
	input [31:0] instruction, dd
	);
	wire [2:0] rfw_d_c, ALU_c;
	wire jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c;
	wire zero;
	wire [5:0] opcode;
	controller ctrlr(rfw_d_c, ALU_c, jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, zero, opcode);
	datapath dpath(zero, opcode, ALU_out, dmwd, pc_out, clk, rst, jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, rfw_d_c, ALU_c, instruction, dd);
endmodule