module controller(
	output reg [2:0] rfw_d_c, ALU_c ,
	output reg jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c,ALU_in_c, data_mem_write_en,
	input zero,
	input [5:0] opcode
	);
	parameter [5:0] ADD = 0, ADDI = 1, SUB = 2, AND = 3, OR = 4, XOR = 5, SLT = 6, SLTI = 7, MULT = 8, DIV = 9, LW = 10, SW = 11, LUI = 12, MFLO = 13, MFHI = 14, J = 15, JAL = 16, JR = 17, BEQ = 18, BNE = 19;
	always@(opcode)begin
		case (opcode)
			ADD		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0110z00001000;
			ADDI	: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0010z10001000;
			SUB		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0110z00001001;
			AND		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0110z00001010;
			OR		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0110z00001011;
			XOR		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0110z00001100;
			SLT		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0110z00001101;
			SLTI	: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0010z10001101;
			MULT	: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0z010z0zzzzzz;
			DIV		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0z011z0zzzzzz;
			LW		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0010z10000000;
			SW		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0z00z11zzz000;
			LUI		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0010zz0110zzz;
			MFLO	: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0010zz0011zzz;
			MFHI	: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'bz0010zz0100zzz;
			J		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'b01z00zz0zzzzzz;
			JAL		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'b01010zz0010zzz;
			JR		: {jval_c, pc_in_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 14'b11z00zz0zzzzzz;
			BEQ		: begin
					{jval_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 13'b0z00z00zzz001;
					pc_in_c <= zero;
			end
			BNE		: begin
					{jval_c, rfw_a_c, rf_write_en, hilo_en, hilo_c, ALU_in_c, data_mem_write_en, rfw_d_c, ALU_c} = 13'b0z00z00zzz001;
					pc_in_c <= ~zero;
			end
		endcase
	end
endmodule