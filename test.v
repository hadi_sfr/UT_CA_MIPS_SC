/*
	CA S96
	CA2

	Hadi Safari
	MohammadReza Tayaranian
*/

`timescale 1ns/1ns
`include "microcontroller.v"

module test();
	reg clk, rst;
	reg [31:0] temp;
	microcontroller ahmadinejad_ir(clk, rst);
  	initial begin rst = 1; clk = 0; #120 rst = 0; end
  	always #50 clk = ~clk;

  	integer db, rs, i;
  	initial begin
	    db = $fopen("test_ta.bin", "r");
	    for(i = 0; i < 18; i = i + 1)begin
	      rs = $fscanf(db, "%b\n", temp[31:0]);
	      {ahmadinejad_ir.inst_mem.data[4*i], ahmadinejad_ir.inst_mem.data[4*i + 1], ahmadinejad_ir.inst_mem.data[4*i + 2], ahmadinejad_ir.inst_mem.data[4*i + 3]} = temp;
	    end
	    $fclose(db);
	    db = $fopen("data_ta.txt", "r");
	    for(i = 0; i < 1000; i = i + 1)begin
	      // rs = $fscanf(db, "%d\n", temp[31:0]);
	      temp = 32'd0;
	      {ahmadinejad_ir.data_mem.data[4*i], ahmadinejad_ir.data_mem.data[4*i + 1], ahmadinejad_ir.data_mem.data[4*i + 2], ahmadinejad_ir.data_mem.data[4*i + 3]} = temp;
	    end
	    $fclose(db);
	end

	integer k;
	reg [31:0] ti;
	initial begin
		//print inst mem
		$display("\n\t\tinstruction mem\n__________________________________________________");
		for(k = 0; k < 20; k = k + 1)begin
			ti = {ahmadinejad_ir.inst_mem.data[4*k], ahmadinejad_ir.inst_mem.data[4*k + 1], ahmadinejad_ir.inst_mem.data[4*k + 2], ahmadinejad_ir.inst_mem.data[4*k + 3]};
			$display("%4d : (%d) %b %b", k + 1, ti[31:26], ti[31:26], ti[26:0]);
		end
		//print data mem
		$display("\n\t\tdata mem (initialization)\n__________________________________________________");
		for(k = 0; k < 25; k = k + 1)
			$display("%4d : %d", k + 1, {ahmadinejad_ir.data_mem.data[4*k], ahmadinejad_ir.data_mem.data[4*k + 1], ahmadinejad_ir.data_mem.data[4*k + 2], ahmadinejad_ir.data_mem.data[4*k + 3]});
	end
	always @(ahmadinejad_ir.processor.dpath.pc.w)begin
		if(ahmadinejad_ir.processor.dpath.pc.w / 4 + 1 == 14)begin
			k = ahmadinejad_ir.processor.dpath.pc.w / 4;
			$display("\n\t\treg file (initialization)\n__________________________________________________");
			ti = {ahmadinejad_ir.inst_mem.data[4*k], ahmadinejad_ir.inst_mem.data[4*k + 1], ahmadinejad_ir.inst_mem.data[4*k + 2], ahmadinejad_ir.inst_mem.data[4*k + 3]};
			$display("%4d : (%d) %b %b", k + 1, ti[31:26], ti[31:26], ti[26:0]);
			for(k = 0; k < 9; k = k + 1)
				$display("%d : %d", k, ahmadinejad_ir.processor.dpath.regfile.data[k]);
			$display("hi : %d",ahmadinejad_ir.processor.dpath.hi.w);
			$display("lo : %d",ahmadinejad_ir.processor.dpath.lo.w);
			$display("%d", ahmadinejad_ir.processor.dpath.hilo_en);
		end
		if(ahmadinejad_ir.processor.dpath.pc.w / 4 + 1 > 18)begin
			$display("\n\t\treg file (initialization)\n__________________________________________________");
			for(k = 0; k < 9; k = k + 1)
				$display("%d : %d", k, ahmadinejad_ir.processor.dpath.regfile.data[k]);
			//print data mem
			$display("\n\t\tdata mem (result)\n__________________________________________________");
			for(k = 0; k < 25; k = k + 1)
				$display("%4d : %d", k + 1, {ahmadinejad_ir.data_mem.data[4*k], ahmadinejad_ir.data_mem.data[4*k + 1], ahmadinejad_ir.data_mem.data[4*k + 2], ahmadinejad_ir.data_mem.data[4*k + 3]});
			$display("hi : %d",ahmadinejad_ir.processor.dpath.hi.w);
			$display("lo : %d",ahmadinejad_ir.processor.dpath.hi.w);
			$finish;
		end
	end
    // initial #1250000 $finish;

    //for test
    // always begin #100 //if(ahmadinejad_ir.processor.dpath.pc.w / 4 + 1 == 20)//if(ahmadinejad_ir.inst_mem.instruction[31:26] == 6'd19)
    	// $display("%b (%2d) : %b", ahmadinejad_ir.processor.dpath.pc.w, ahmadinejad_ir.processor.dpath.pc.w / 4 + 1, 
    	// ahmadinejad_ir.processor.dpath.zero,
    	// ahmadinejad_ir.processor.dpath.regfile.data[]
    	// ahmadinejad_ir.processor.dpath.npc_e,
    	// ahmadinejad_ir.processor.dpath.pc.w
    	// ahmadinejad_ir.inst_mem.instruction,
    	// {ahmadinejad_ir.inst_mem.data[ahmadinejad_ir.processor.dpath.pc.w], ahmadinejad_ir.inst_mem.data[ahmadinejad_ir.processor.dpath.pc.w + 1], ahmadinejad_ir.inst_mem.data[ahmadinejad_ir.processor.dpath.pc.w + 2], ahmadinejad_ir.inst_mem.data[ahmadinejad_ir.processor.dpath.pc.w + 3]},
    	// ); #0 ;end
endmodule
