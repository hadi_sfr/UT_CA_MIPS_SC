module instruction_mem(output [31:0] instruction, input [11:0] pc, input clk);
	reg [7:0] data [0:5000];
	// always@(posedge clk)
		// instruction <= {data[pc], data[pc + 1], data[pc + 2], data[pc + 3]};
	assign instruction = {data[pc], data[pc + 1], data[pc + 2], data[pc + 3]};
endmodule